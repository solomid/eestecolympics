CPP_FILES := $(wildcard src/*.cpp)
HPP_FILES := $(wildcard headers/*.hpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))

main.exe: $(OBJ_FILES)
	   g++ -o $@ $^

obj/%.o: src/%.cpp
	   g++ -g -std=c++11 -c -o $@ $<
clean:
	rm obj/*
	rm main.exe
