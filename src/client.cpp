#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <iostream>
#include <vector>
#include "../headers/map.hpp"
#include "../headers/basicAI.hpp"
using namespace std;

int main(int argc, char *argv[])
{
    int sockfd = 0, n = 0;
    char recvBuff[1024];
    struct sockaddr_in serv_addr; 

    if(argc != 2)
    {
        printf("\n Usage: %s <ip of server> \n",argv[0]);
        return 1;
    } 

    memset(recvBuff, '0',sizeof(recvBuff));
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    } 

    memset(&serv_addr, '0', sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(10000); 

    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\n Error : Connect Failed \n");
        return 1;
    } 

    int id;
    int currentMove;
    int rows, cols;
    int agrMode;
    int maxMove;
    BasicAI ai(*Map::getInstance());
    n = read(sockfd, &id, sizeof(int));
    Player::players[0].id = id;
    cout << "id is " << id << endl;
    while ( (n = read(sockfd, &currentMove, sizeof(int))) > 0)
    {
        vector<vector<int>> map;
        read(sockfd, &agrMode, sizeof(int));
        read(sockfd, &maxMove, sizeof(int));
        read(sockfd, &rows, sizeof(int));
        read(sockfd, &cols, sizeof(int));
        for (int i = 0; i < rows; i++)
        {
            map.push_back(vector<int>());
            for (int j = 0; j < cols; j++)
            {
                int mapElement;
                read(sockfd, &mapElement, sizeof(int));
                map[i].push_back(mapElement);
                for (int k = 0; k < 8; k++)
                {
                    if (mapElement & (1 << k))
                    {
                        if (Player::players[0].id - 1 == k)
                        {
                            Player::players[0].setX(i);
                            Player::players[0].setY(j);
                        }
                    }
                }
            }
        }

        Map* mp = Map::getInstance();
        mp->setMap(map);
        int move = ai.getResult(); 
        if (currentMove % 4 == 0)
        {
            move = move | (1 << 31);
        }
        write(sockfd, &currentMove, sizeof(int));
        write(sockfd, &move, sizeof(int));  
    } 

    return 0;
}
