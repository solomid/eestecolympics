#include "../headers/flame.hpp"
Flame::Flame(const int & x, const int & y, const int & life) : m_xPosition(-1), m_yPosition(-1), m_lifetime(0)
{
	m_xPosition = x;
	m_yPosition = y;
	m_lifetime = life;
}
Flame::~Flame()
{

}

void Flame::getPosition(int & x, int & y) const
{
	x = m_xPosition;
	y = m_yPosition;
}

int Flame::getLifetime() const
{
	return m_lifetime;
}

void Flame::updateLifetime()
{
	m_lifetime --;
}
int Flame::getXPos() const
{
	return m_xPosition;

}
int Flame::getYPos() const
{
	return m_yPosition;
}