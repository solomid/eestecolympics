#include "../headers/position.hpp"
Position::Position()
{
	m_xPosition = -1;
	m_yPosition = -1;

}

Position::~Position()
{

}
Position::Position(int x, int y)
{
	m_xPosition = x;
	m_yPosition = y;
}
void Position::getPosition(int & x, int & y) const
{
	x = m_xPosition;
	y = m_yPosition;
}

void Position::setPosition(const int & x, const int & y)
{
	m_xPosition = x;
	m_yPosition = y;
}
