#include "../headers/basicAI.hpp"
#include "../headers/flame.hpp"
#include "../headers/position.hpp"
#include "../headers/player.hpp"
#include <cmath>
#include <queue>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <set>
using namespace std;
BasicAI::BasicAI(Map& map) : m_map(map), m_player(Player::players[0])
{
	blockingRange.first = 0;
	blockingRange.second = 12;
}
void BasicAI::setMap(Map& newMap)
{
	//m_map = newMap;

}
bool BasicAI::isFree(int x, int y, int turn)
{
	Cell curCell = m_map.getMatrix()[x][y];
	if (!curCell.isWall() && curCell.getFlameActive() - turn <= 0 && 
		(curCell.getBombPresent() == 0 || curCell.getBombPresent() - turn <= -3 ))
		return true;
	else
		return false;
}
bool BasicAI::isSafe(int x, int y, int turn)
{
	Cell curCell = m_map.getMatrix()[x][y];
	if (!curCell.isWall() && curCell.getFlameActive() - turn <= 0 && 
		(curCell.getBombPresent() == 0 || curCell.getBombPresent() - turn <= -3 ))
		return true;
	else
		return false;

}

vector<Position> BasicAI::getShortestPath(Position& pos)
{
	int nn = m_map.getN();
	int mm = m_map.getM();
	vector<int> fathers(m_map.getM() * m_map.getN());
	vector<vector<bool> > visited(m_map.getM(), vector<bool>(m_map.getN(), false));
	queue<pair<Position, int> > q;
	int x, y, indicator, turn;
	Position curPos;
	Cell curCell;
	q.push(make_pair(Position(m_player.getX(), m_player.getY()), 0));
	visited[m_player.getX()][m_player.getY()] = true;
	while (x != pos.getXPos() || y != pos.getYPos())
	{

		if (q.empty())
		{
			cout << "ERROR NOT FOUND PATH\n ";
			return vector<Position>();
		}	
		turn = q.front().second;
		x = q.front().first.getXPos();
		y = q.front().first.getYPos();
		cout << "(" << x << " " << y << ")  \n";
		q.pop();
		
		indicator = m_map.getN() * x + y;
		if (m_map.inBounds(x+1,y) && !visited[x+1][y] && isSafe(x+1, y, turn+1))
		{
			q.push(make_pair(Position(x+1,y), turn+1));
			visited[x+1][y] = true;
			fathers[(x+1) * m_map.getN() + y] = indicator;
		}	
		
		if (m_map.inBounds(x-1, y) && !visited[x-1][y] && isSafe(x-1, y, turn+1))
		{
			q.push(make_pair(Position(x-1,y), turn+1));
			visited[x-1][y] = true;
			fathers[(x-1) * m_map.getN() + y] = indicator;

		}	
		
		if (m_map.inBounds(x,y+1) && !visited[x][y+1] && isSafe(x, y+1, turn+1) )
		{
			q.push(make_pair(Position(x,y+1), turn+1));
			visited[x][y+1] = true;
			fathers[x * m_map.getN() + y + 1] = indicator;

		}	
		
		if (m_map.inBounds(x,y-1) && !visited[x][y-1] && isSafe(x, y-1, turn+1))
		{
			q.push(make_pair(Position(x,y-1), turn+1));
			visited[x][y-1] = true;
			fathers[x * m_map.getN() + y - 1] = indicator;

		}	
		
	}
	vector<Position> path;
	x = pos.getXPos();
	y = pos.getYPos();
	while (x != m_player.getX() || y != m_player.getY())
	{
		path.push_back(Position(x,y));
		indicator = fathers[x * m_map.getN() + y];
		y = indicator % m_map.getN();
		x = indicator / m_map.getN();
	}

	return path;

}

int BasicAI::pathLen(int desX, int desY)
{
	
	vector<vector<bool> > visited(m_map.getN(), vector<bool>(m_map.getM(), false));
	queue<pair<Position, int> > q;
	q.push(make_pair(Position(m_player.getX(), m_player.getY()), 0));
	visited[m_player.getX()][m_player.getY()] = true;
	int turn, x, y;
	while (x != desX || y != desY)
	{

		if (q.empty())
		{
			return -1;
		}	
		turn = q.front().second;
		x = q.front().first.getXPos();
		y = q.front().first.getYPos();
		q.pop();
		if (m_map.inBounds(x+1,y) && !visited[x+1][y] && isSafe(x+1, y, turn+1))
		{
			q.push(make_pair(Position(x+1,y), turn+1));
			visited[x+1][y] = true;
		}	
		
		if (m_map.inBounds(x-1, y) && !visited[x-1][y] && isSafe(x-1, y, turn+1))
		{
			q.push(make_pair(Position(x-1,y), turn+1));
			visited[x-1][y] = true;
		}	
		
		if (m_map.inBounds(x,y+1) && !visited[x][y+1] && isSafe(x, y+1, turn+1) )
		{
			q.push(make_pair(Position(x,y+1), turn+1));
			visited[x][y+1] = true;
		}	
		
		if (m_map.inBounds(x,y-1) && !visited[x][y-1] && isSafe(x, y-1, turn+1))
		{
			q.push(make_pair(Position(x,y-1), turn+1));
			visited[x][y-1] = true;
		}	
		
	}
	return turn;

}
void BasicAI::updateCells()
{
    Map* mp = Map::getInstance();
    vector<vector<Cell>>& map = mp->getMatrix();
    int n = map.size();
    int m = map[0].size();
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            map[i][j].updateCellMobility();
        }
    }
}

int BasicAI::getResult()
{
    Map* mp = Map::getInstance();
    mp->setQuadrant(Player::players[0].getX(), Player::players[0].getY());
    updateCells();
    mp->updateSafeCells();
    vector<vector<Cell>>& map = mp->getMatrix();
    int n = map.size();
    int m = map[0].size();
    vector<vector<int>> dist(n, vector<int>(m, 0));
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            dist[i][j] = pathLen(i, j);
        }
    }

    double maxDist = 2 * sqrt(n^2 + m^2);

    set<pair<double, int>> costs;
    for (int i = 0; i < 4; i++)
    {
        double p = 1;
        for (auto v : mp->quadrant[i])
        {
            double P = ((double)v->getMobility() / (double) 100);
            P = P + (1 - P) * ((double)dist[v->x][v->y] / maxDist); 
            p *= ((double)v->getMobility() / (double) 100);
        }
        cout << "prob is " << p << endl;
        costs.insert(make_pair(p, i));
    }
    
    int playerX = Player::players[0].getX();
    int playerY = Player::players[0].getY();

	int a[4] = {-1, 0, 1, 0};
	int b[4] = {0, -1, 0, 1};

	set<pair<int,int>> val;

	for (int i = 0; i < 4; i++)
	{
		val.insert(make_pair(checkDeadEnd(playerX, playerY, playerX + a[i], playerY + b[i]), i));
	}	

    
    set<pair<double,int>> keep;
    if (val.rbegin()->first <= blockingRange.second || val.begin()->first >= blockingRange.second)
    	;
    else
    {
    	for (auto it = costs.begin(); it != costs.end(); ++it)
    	{
	  		for (auto v : val)
    		{
    			if (it->second == v.second && v.first >= blockingRange.second)
    			{
    				keep.insert(*it);
    			}
    		}


    	}
    	costs.clear();
    	for (auto it = keep.begin(); it != keep.end(); ++it)
    	{
    		costs.insert(*it);
    	}
    }


    while (!costs.empty())
    {
        int dx, dy;
        auto fst = *costs.begin();
        costs.erase(costs.begin());
        int ind = fst.second;
        if (ind % 2 == 0)
        {
            dy = 0;
            dx = ind - 1;
        }
        else
        {
            dx = 0;
            dy = ind - 2;
        }
        cout << "dx=" << dx << " dy=" << dy << endl;
        
        

        if (!map[playerX + dx][playerY + dy].isSafe() || 
            map[playerX + dx][playerY + dy].isWall() || 
            map[playerX + dx][playerY + dy].getBombPresent() > 0 ||
            map[playerX + dx][playerY + dy].getFlameActive() > 1)
        {
            continue;
        }
        else
        {
            Player::players[0].setX(playerX + dx);
            Player::players[0].setY(playerY + dy);
            if (ind == 3)
                return 1;
            else return ind + 2;
        }
    }
    return 0;
}

int BasicAI::checkDeadEnd(int curX, int curY, int ngbX, int ngbY)
{
	vector<vector<bool> > visited(m_map.getN(), vector<bool>(m_map.getM(), false));
	queue<pair<pair<int,int>, int> > q;
	visited[ngbX][ngbY] = true;
	visited[curX][curY] = true;
	q.push(make_pair(make_pair(ngbX,ngbY), 1));
	pair<pair<int,int>, int> pos;
	int x,y;
	x = ngbX;
	y = ngbY;
	int turn;
	
	while (!q.empty())
	{
		pos = q.front();
		x = pos.first.first;
		y = pos.first.second;
		turn = pos.second;
		q.pop();
		
		
		if (m_map.inBounds(x+1, y) && !visited[x+1][y] && isFree(x+1,y, turn+1))
		{
			q.push(make_pair(make_pair(x+1,y), turn+1));
			visited[x+1][y] = true;
		}
		
		if (m_map.inBounds(x-1, y) && !visited[x-1][y] && isFree(x-1,y,turn+1))
		{
			q.push(make_pair(make_pair(x-1,y), turn+1));
			visited[x-1][y] = true;
		}
		if (m_map.inBounds(x, y+1) && !visited[x][y+1] && isFree(x,y+1,turn+1))
		{
			q.push(make_pair(make_pair(x,y+1), turn+1));
			visited[x][y+1] = true;
		}
		if (m_map.inBounds(x, y-1) && !visited[x][y-1] &&isFree(x,y-1,turn+1))
		{
			q.push(make_pair(make_pair(x,y-1), turn+1));
			visited[x][y-1] = true;
		}
		
	}
	return turn;
}



