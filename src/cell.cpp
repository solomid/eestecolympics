#include "../headers/cell.hpp"
#include "../headers/map.hpp"
#include "../headers/player.hpp"
#include <iostream>
#include <vector>
    Cell::Cell(int info)
	{
        Map* mp = Map::getInstance();
		m_mobility = 0;
		safe = true;
		m_playerPresent.resize(2, false);
		for (int i = 0; i < 8; i++)
		{
			if (info & (1 << i))
			{
                if (Player::players[0].id == i)
				    m_playerPresent[0] = true;
                else
                    m_playerPresent[1] = true;
			}
		}
			if (info & (1 << 15))
				m_isWall = true;
			else
				m_isWall = false;

		m_flameActive = (info << 8) >> 24;
		m_bombPresent = info >> 24;

		m_M = mp->getM();
		m_N = mp->getN();

	}
	bool Cell::isWall() const
	{
		return m_isWall;
	}
	int Cell::getFlameActive() const
	{
		return m_flameActive;
	}
	int Cell::getBombPresent() const
	{
		return m_bombPresent;
	}
	void Cell::printCell()
	{
		std::cout << "PlayersPresent: ";
		for (int i = 0; i < 8; i++)
		{
			if (m_playerPresent[i])
				std::cout << i << " ";

		}
		std::cout << '\n';
		if (m_isWall)
			std::cout << "Is a wall\n";
		std::cout << "Flame active: " << m_flameActive << "\n";
		std::cout << "Bomb Active: " << m_bombPresent << "\n";
	}


void Cell::updateCellMobility()
{
    Map* mp = Map::getInstance();
	// wall, bomb, flame in cell
    if(m_isWall)
    {
        m_mobility = 90;
        return;
    }
	if(m_bombPresent || m_flameActive) 
	{
		m_mobility = 70;
		return;
	}

	// determine if bombs near cell or cell shielded by wall

	m_mobility = 100;
	double bombTime;	

	// north
	int newMobility;

	for(int i = 1; i<=7 && x - i>=0; i++)
	{

		newMobility = 100;
		if(mp->getMatrix()[x-i][y].isWall()) break;	// daca e shielded de wall la revedere
		
		bombTime = mp->getMatrix()[x-i][y].getBombPresent();
		
		if(bombTime > 0.2)											// daca am bomba, vad cum imi influenteaza mobilitatea
		{
			/*bombTime = (double) bombTime / 2.;
			bombTime = bombTime * (double) i / 7.0;*/
			//newMobility = (int) (bombTime * 100);
		    newMobility = 80;
		}

		if(newMobility < m_mobility) m_mobility = newMobility;
	}
	
	

	// south
	
	for(int i = 1; i<=7 && x + i < m_M; i++)
	{
		newMobility = 100;	
		if(mp->getMatrix()[x+i][y].isWall()) break;

		bombTime = mp->getMatrix()[x+i][y].getBombPresent();

		if(bombTime > 0.2)											// daca am bomba, vad cum imi influenteaza mobilitatea
		{
			/*bombTime = (double) bombTime / 2.;
			bombTime = bombTime * (double) i / 7.0;*/
			//newMobility = (int) (bombTime * 100);
		    newMobility = 80;
		}

		if(newMobility < m_mobility) m_mobility = newMobility;
	}

	

	// west
	
	for(int i = 1; i<=7 && y - i >= 0; i++)
	{
		newMobility = 100;
		if(mp->getMatrix()[x][y-i].isWall()) break;

		bombTime = mp->getMatrix()[x][y-i].getBombPresent();

		if(bombTime > 0.2)											// daca am bomba, vad cum imi influenteaza mobilitatea
		{
			/*bombTime = (double) bombTime / 2.;
			bombTime = bombTime * (double) i / 7.0;*/
			//newMobility = (int) (bombTime * 100);
		    newMobility = 80;
		}

		if(newMobility < m_mobility) m_mobility = newMobility;
	}

	

	// east
	
	for(int i = 1; i<=7 && y + i < m_N; i++)
	{
		newMobility = 100;
		if(mp->getMatrix()[x][y+i].isWall()) break;

		bombTime = mp->getMatrix()[x][y+i].getBombPresent();

		if(bombTime > 0.2)											// daca am bomba, vad cum imi influenteaza mobilitatea
		{
			/*bombTime = (double) bombTime / 2.;
			bombTime = bombTime * (double) i / 7.0;*/
			//newMobility = (int) (bombTime * 100);
		    newMobility = 80;
        }

		if(newMobility < m_mobility) m_mobility = newMobility;
	}

	return;
}
	
int Cell::getMobility() const
{
	return m_mobility;
}

bool Cell::isSafe() const
{
	return safe;
}
