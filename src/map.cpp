#include "../headers/map.hpp"
#include <algorithm>
#include <set>
using namespace std; 

void Map::setMap(vector<vector<int>>& newMap)
{
    m_matrix.clear();
    int m = newMap.size();
    int n = newMap[0].size();
    m_N = m;
    m_M = n;
    for (int i = 0; i < m; i++)
    {
        m_matrix.push_back(vector<Cell>());
        for (int j = 0; j < n; j++)
        {
            m_matrix[i].push_back(Cell(newMap[i][j]));     
            m_matrix[i][j].x = i;
            m_matrix[i][j].y = j;
        }
    }

    updateBombs();
}

void Map::updateBombs()
{
    m_bombs.clear();
    Cell* curCell;
    for (int i = 0; i < m_N; i ++)
        for (int j = 0; j < m_M; j++)
        {
            curCell = &(m_matrix[i][j]);
            if (curCell->getBombPresent() > 0)
                m_bombs.insert(make_pair(curCell->getBombPresent(), curCell));

        }
    auto earlyBomb = m_bombs.begin(),curElement = m_bombs.begin();
    int x, y;
    int lifetime;
    while (!m_bombs.empty())
    {
        earlyBomb = m_bombs.begin();
        m_bombs.erase(m_bombs.begin());
        x = earlyBomb->second->x;
        y = earlyBomb->second->y;
        lifetime = earlyBomb->first;
        //look top
        for (int i = x; i >= max(0, x - 6); i--)
        {
            if (m_matrix[i][y].isWall())
                break;
            if (m_matrix[i][y].getBombPresent() > 0)
            {
                curElement = m_bombs.find(make_pair(m_matrix[i][y].getBombPresent(), &(m_matrix[i][y])));
                if (curElement != m_bombs.end())
                {
                    m_bombs.erase(curElement);
                    m_matrix[i][y].setBombLife(min(m_matrix[i][y].getBombPresent() + 1, lifetime));
                    m_bombs.insert(make_pair(m_matrix[i][y].getBombPresent(), &(m_matrix[i][y])));
                }
            }        


        }
        //look bottom
        for (int i = x; i <= min(x+6,m_M-1); i++)
        {
            if (m_matrix[i][y].isWall())
                break;
            if (m_matrix[i][y].getBombPresent() > 0)
            {
                curElement = m_bombs.find(make_pair(m_matrix[i][y].getBombPresent(), &(m_matrix[i][y])));
                if (curElement != m_bombs.end())
                {
                    m_bombs.erase(curElement);
                    m_matrix[i][y].setBombLife(min(m_matrix[i][y].getBombPresent() + 1, lifetime));
                    m_bombs.insert(make_pair(m_matrix[i][y].getBombPresent(), &(m_matrix[i][y])));
                }
            }        


        }
        //look left
        for (int j = y; j >= min(0, y - 6); j--)
        {
            if (m_matrix[x][j].isWall())
                break;
            if (m_matrix[x][j].getBombPresent() > 0)
            {
                curElement = m_bombs.find(make_pair(m_matrix[x][j].getBombPresent(), &(m_matrix[x][j])));
                if (curElement != m_bombs.end()) 
                {
                    m_bombs.erase(curElement);
                
                    m_matrix[x][j].setBombLife(min(m_matrix[x][j].getBombPresent() + 1, lifetime));
                    m_bombs.insert(make_pair(m_matrix[x][j].getBombPresent(), &(m_matrix[x][j])));
                }
            }        


        }

        //look right
        for (int j = y; j <= min(m_M-1, y + 6); j++)
        {
            if (m_matrix[x][j].isWall())
                break;
            if (m_matrix[x][j].getBombPresent() > 0)
            {
                curElement = m_bombs.find(make_pair(m_matrix[x][j].getBombPresent(), &(m_matrix[x][j])));
                if (curElement != m_bombs.end()) 
                {
                    m_bombs.erase(curElement);
                
                    m_matrix[x][j].setBombLife(min(m_matrix[x][j].getBombPresent() + 1, lifetime));
                    m_bombs.insert(make_pair(m_matrix[x][j].getBombPresent(), &(m_matrix[x][j])));
                }
            }        


        }



    }
}
void Map::setQuadrant(int x,int y)
{
    quadrant.clear();
    quadrant.resize(4);
    int n = m_matrix.size();
    int m = m_matrix[0].size();
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if ((n - i) >= f1(j, x, y) && (n - i) >= f2(j, x, y))
                quadrant[0].push_back(&m_matrix[i][j]);
            if ((n - i) >= f1(j, x, y) && (n - i) <= f2(j, x, y));
                quadrant[1].push_back(&m_matrix[i][j]);
            if ((n - i) <= f1(j, x, y) && (n - i) <= f2(j, x, y))
                quadrant[2].push_back(&m_matrix[i][j]);
            if ((n - i) <= f1(j, x, y) && (n - i) >= f2(j, x, y))
                quadrant[3].push_back(&m_matrix[i][j]);
        }
    }
}
bool Map::inBounds(Position& pos)
{
    return (pos.getXPos() >= 0 && pos.getXPos() < m_M && 
            pos.getYPos() >= 0 && pos.getYPos() < m_N);
}
bool Map::inBounds(int x, int y)
{
    return (x >= 0 && x < m_M && y >= 0 && y < m_N);
}
Map* Map::m_instance = 0;

void Map::updateSafeCells()
{
    for(auto v : m_bombs)
    {
        if(v.second->getBombPresent() == 1)
        {
            int x = v.second->x;
            int y = v.second->y;

            m_matrix[x][y].safe = false;

            for(int j = 1; j <=6; j++)
            {
                if(x+j < m_M)
                    m_matrix[x+j][y].safe = false;
                if(y+j < m_N)
                    m_matrix[x][y+j].safe = false;
                if(x-j >= 0)
                    m_matrix[x-j][y].safe = false;
                if(y-j >= 0)
                    m_matrix[x][y-j].safe = false;
            }
        }
    }
}
