#include "../headers/bomb.hpp"
Bomb::Bomb(const int & x, const int & y, const int & life)
{
	m_xPosition = x;
	m_yPosition = y;
	m_lifetime = life;
}
Bomb::~Bomb()
{

}

int Bomb::getxPosition() const
{
	return m_xPosition;
}

int Bomb::getyPosition() const
{
	return m_yPosition;
}

int Bomb::getLifetime() const
{
	return m_lifetime;
}

void Bomb::updateLifetime()
{
	m_lifetime --;
}
