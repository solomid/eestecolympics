#ifndef __DEFENSIVEAI_HPP__
#define __DEFENSIVEAI_HPP__
#include "position.hpp"
#include "basicAI.hpp"
#include "move.hpp"

	class DefensiveAI : public BasicAI
	{
	private:
		DefensiveAI(Map& map) : BasicAI(map)
		{

		}
		
	public:

		std::vector<Move> getSafeMoves(int,int);

	};

#endif