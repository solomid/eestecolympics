#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__
#include<vector>
using namespace std;
class Player 
{
public:
	Player();
	int getX() const  {return m_xPosition;}
	int getY() const {return m_yPosition;}
    void setX(int x)
    {
        m_xPosition = x;
    }

    void setY(int y)
    {
        m_yPosition = y;
    }

    static vector<Player> players;

    int id;


private:
	int m_xPosition;
	int m_yPosition;

	unsigned int m_ID;
	unsigned int m_bombsPlaced;


	


};
#endif
