#ifndef __BASICAI_HPP__
#define __BASICAI_HPP__
#include "strategy.hpp"
#include <vector>
#include "position.hpp"
#include "map.hpp"
#include "player.hpp"
class BasicAI
{
	protected:
		Map& m_map;
		Player& m_player;
		std::vector<Map> m_tenMapConfigs;
	public:
		BasicAI(Map& _map);
		void setMap(Map& newMap);
		void computePositions();
		std::vector<Position>& getSafePositions() ;
		std::vector<Position>& getDeathPositions() ;
		std::vector<Position>& getThreatenedPositions() ;
		std::vector<Position> getShortestPath(Position& pos);
		
		bool isSafe(int x, int y, int turn);
		bool isFree(int x, int y, int turn);
		int pathLen(int x, int y);

		void updateConfigMaps();
		int checkDeadEnd(int curX, int curY, int ngbX, int ngbY);
        void updateCells();
        int getResult();
        pair<int,int> blockingRange;

};
#endif
