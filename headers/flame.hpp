#ifndef __FLAME_HPP__
#define __FLAME_HPP__
class Flame
{

	int m_xPosition;
    int m_yPosition;
    int m_lifetime;


public:
    Flame(const int & x, const int & y, const int & life);
    ~Flame();



    void getPosition(int &, int &) const;
    int getXPos() const;
    int getYPos() const;
    int getLifetime() const;
    void updateLifetime();
};
#endif
