#ifndef __BOMB_HPP__
#define __BOMB_HPP__
class Bomb
{
	int m_xPosition;
	int m_yPosition;
	int m_lifetime;
public:
	Bomb(const int &, const int &, const int &);
	~Bomb();



    int getxPosition() const; 
	void getPosition(int &, int &) const;
	int getLifetime() const;
    int getyPosition() const;
	void updateLifetime();

};
#endif
