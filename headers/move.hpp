#ifndef __MOVE_HPP__
#define __MOVE_HPP__

	enum Move
	{
		HOLD,
		RIGHT,
		UP,
		LEFT,
		DOWN
	};

#endif