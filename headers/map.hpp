
#ifndef __MAP_HPP__
#define __MAP_HPP__
#include "../headers/position.hpp"
#include "../headers/player.hpp"
#include "../headers/bomb.hpp"
#include "../headers/flame.hpp"
#include "../headers/cell.hpp"
#include <vector>
#include <set>
#include <queue>

class Map
{

    private:

        static Map* m_instance;
        Position m_pos;
        int m_N, m_M;
        int m_agressiveMode;
        int m_nrMutare;
        int m_mutareMaxima;
        std::vector<std::vector<Cell> > m_matrix;
        std::vector<Player> m_players;
        std::set<std::pair<int, Cell*> > m_bombs;
        std::vector<Flame> m_flames;

        Map() : BOMB_LIFE(10) {}

    public: 
        const int BOMB_LIFE;
        static Map* getInstance()
        {
            if (!m_instance)
                m_instance = new Map();
            return m_instance;
        }

        std::vector<Player>& getPlayers() {return m_players;}
        std::set<std::pair<int, Cell*> >& getBombs(){return m_bombs;}
        std::vector<Flame>& getFlames(){return m_flames;}
        void setMap(std::vector<std::vector<int>>& newMap);
        void setQuadrant(int i, int j);
        std::vector<std::vector<Cell> >& getMatrix() {return m_matrix;}
        int getM() const { return m_M;}
        int getN() const { return m_N;}
        bool inBounds(Position&);
        bool inBounds(int x, int y);
        void updateBombs();
        std::vector<std::vector<Cell*>> quadrant;

        int f1(int x, int i, int j)
        {
            int m = m_matrix.size() -i -j;
            return x + m; 
        }

        int f2(int x, int i, int j)
        {
            int m = m_matrix.size() - i + j;
            return -x + m;
        }

        void updateSafeCells();

};
#endif

