#ifndef __CELL_HPP__
#define __CELL_HPP__
#include <vector>
class Cell
{
    private:
        bool m_isWall;
        std::vector<bool> m_playerPresent;
        int m_flameActive;
        int m_bombPresent;
        int m_mobility;
        int m_M;
        int m_N;
        

    public:

        Cell(){};
        
        Cell(int info);
        bool isWall() const;
        std::vector<bool>& getPlayersPresent() const;
        int getFlameActive() const;
        int getBombPresent() const;
        int getMobility() const;
        void printCell();
        void updateCellMobility();

        void setBombLife(int x) { m_bombPresent = x;};

        bool isSafe() const;
        bool safe;

        int x;
        int y;
};
#endif
