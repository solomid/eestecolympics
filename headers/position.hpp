#ifndef __POSITION_HPP__
#define __POSITION_HPP__
class Position
{
	int m_xPosition;
	int m_yPosition;
public:
	Position();
	Position(const Position& pos) 
	{
		m_xPosition = pos.getXPos(); 
		m_yPosition = pos.getYPos();
	}
	Position(int , int );
	~Position();

	int getXPos() const {return m_xPosition;};
	int getYPos() const {return m_yPosition;};
	
	void getPosition(int & , int &) const;
	void setPosition(const int & , const int & );
};

#endif
