#ifndef __STRATEGY_HPP__
#define __STRATEGY_HPP__
#include "move.hpp"


	class Strategy
	{
	public:
		virtual Move getMove() = 0;
		virtual bool placeBomb() = 0;
		virtual ~Strategy() = 0;
	};
#endif